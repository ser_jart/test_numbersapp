export function filterByMonthAndDate(arr:string[]):string[] {
    const dateRegex = /((January|February|March|April|May|June|July|August|September|October|November|December)\s\d{1,2}(st|nd|rd|th))/;

    return arr.filter(item => dateRegex.test(item))
}