type Response = {
    date: string;
    text: string;
    number: number;
    found: boolean;
    type: string;
};

export async function getRequest(url: string, headers:{} = {}):Promise<Response>  {
    try {
        const response = await fetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                ...headers,
            },
        });

        if (!response.ok) {
            throw new Error(`HTTP error! Status: ${response.status}`);
        }

        return await response.json();
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
}