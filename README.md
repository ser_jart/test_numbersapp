# API Test Project

This is a test project for API testing based on [http://numbersapi.com/](http://numbersapi.com/).

## Project Overview

The project is written in TypeScript and uses the Jest testing framework. 

## Installation

To install the required dependencies, run the following command:

```
npm install
```
## Running test

To running tests, run the following command:

```
npm test
```