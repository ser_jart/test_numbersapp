import {getRequest} from "./src/request_helper";
import {filterByMonthAndDate} from "./src/validate_helper";

let testData: string[];

describe('Filtered array by date from /random/year endpoint ', () => {

    beforeAll(async ()=>{
        const requests = Array.from({ length: 100 }, () => getRequest('http://numbersapi.com/random/year'));
        const responses = await Promise.all(requests);
        const textArray =responses.map(response => response.text);

        testData = filterByMonthAndDate(textArray);
    })

    it('should not be empty', () => {
        expect(testData).not.toHaveLength(0);
    })

    it('should be empty', () => {
        expect(testData).toHaveLength(0);
    })
    it('should have length greater than 5', () => {
        expect(testData.length).toBeGreaterThan(5);
    })
    it('should have lengths of elem greater than 10', () => {
        for (const element of testData) {
            expect(element.length).toBeGreaterThan(10);
        }
    })
});